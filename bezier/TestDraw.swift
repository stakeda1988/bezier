//
//  TestDraw.swift
//  bezier
//
//  Created by SHOKI TAKEDA on 2/27/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit

class TestDraw: UIView {
    
    override func drawRect(rect: CGRect) {
        
        let context = UIGraphicsGetCurrentContext()
        let myShadowOffset = CGSizeMake (-10,  15)
        
        CGContextSaveGState(context)
        CGContextAddRect(<#T##c: CGContext?##CGContext?#>, <#T##rect: CGRect##CGRect#>)
        CGContextSetShadow (context, myShadowOffset, 5)
        
        CGContextSetLineWidth(context, 1.0)
        CGContextSetStrokeColorWithColor(context,
            UIColor(red:255/255, green:255/255, blue:255/255, alpha:1).CGColor)
        CGContextMoveToPoint(context, 10, 10)
        CGContextAddCurveToPoint(context, 0, 50, 300, 250, 300, 400)
        CGContextStrokePath(context)
        CGContextRestoreGState(context)
        
    }
}