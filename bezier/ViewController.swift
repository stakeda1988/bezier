//
//  ViewController.swift
//  bezier
//
//  Created by SHOKI TAKEDA on 2/27/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Screen Size の取得
        let screenWidth = self.view.bounds.width
        let screenHeight = self.view.bounds.height
        
        let testDraw = TestDraw(frame: CGRectMake(0, 0, screenWidth, screenHeight))
        self.view.addSubview(testDraw)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}